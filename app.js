require('dotenv').config();

const express = require('express');
const next = require('next');
const pageBinding = require('./middleware/pageBinding');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express();

    server.use(express.urlencoded({ extended: true }));
    server.use(pageBinding);

    server.get('*', handle);
    server.post('*', handle);

    server.listen(process.env.PORT, err => {
        if (err) throw err;
    });
});
