const fs = require('fs');
const path = require('path');
const glob = require('glob');
const ejs = require('ejs');

const pageTemplatePath = path.join(process.cwd(), process.env.TEMPLATES_DIR, 'page.ejs');
const pageTemplate = fs.readFileSync(pageTemplatePath).toString('utf-8');
const pagesRoot = path.join(process.cwd(), 'pages');

module.exports = (req, res, next) => {
    const pathParts = req.path.replace(/^\//, '').split('/');
    const name = pathParts.length > 0 ? pathParts[0] : '';

    if (name) {
        const projectsRoot = path.join(process.cwd(), process.env.PROJECTS_DIR, name);
        const projectMatch = glob.sync('/index.{j,t}s?(x)', { root: projectsRoot });
        const pageMatch = glob.sync(`/${name}.{j,t}s?(x)`, { root: pagesRoot });
        const projectExists = projectMatch.length === 1;
        const pageExists = pageMatch.length === 1;

        if (projectExists && !pageExists) {
            const content = ejs.render(pageTemplate, { projectPath: path.relative(pagesRoot, projectsRoot) });

            fs.writeFileSync(path.join(pagesRoot, `${name}.js`), content);
        }
    }

    console.log('next');

    next();
};
