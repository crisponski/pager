# PAGER

Service for SSR

## Getting started

### Clone project

```shell script
git clone https://gitlab.com/crisponski/pager.git
cd pager
npm install
```

### Additional actions

Copy file `.env.example` and rename to `.env`. Then you can change default variables

By default `PROJECTS_DIR=projects`. So you should create directory `projects`

```
pager
  |- ...
  |- projects
       |- project_1
       |- project_2
       |- ...
```

### Run server

```
npm start
```

🙌
